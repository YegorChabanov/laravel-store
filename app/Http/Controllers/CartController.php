<?php

namespace App\Http\Controllers;

use App\Services\CartService;
use App\Product;

class CartController extends Controller
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $cartWithProductsArray = $this->cartService->getCartWithProducts();
        $totalPrice = $this->cartService->getTotalPriceOfCartProducts();
        return view('cart.index')->with(compact('cartWithProductsArray', 'totalPrice'));
    }

    public function store(Product $product)
    {
        $cart = $this->cartService->addProductToCart($product);

        return back()->withCookie('cart', json_encode($cart))->with(['status' => 'Product successfully added to your cart!']);
    }

    public function destroy(Product $product)
    {
        $cart = $this->cartService->removeProductFromCart($product);

        return back()->withCookie('cart', json_encode($cart));
    }

    public function unset(Product $product)
    {
        $cart = $this->cartService->unsetProductFromCart($product);

        return back()->withCookie('cart', json_encode($cart));
    }
}
