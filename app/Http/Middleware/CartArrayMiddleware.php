<?php

namespace App\Http\Middleware;

use App\Services\CartService;
use Closure;
use Illuminate\Support\Facades\View;

class CartArrayMiddleware
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param CartService $cartService
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cartArray = $this->cartService->getCartArray();
        View::share('cartArray', $cartArray);

        return $next($request);
    }
}
