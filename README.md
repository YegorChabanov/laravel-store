## Docker

```

// in root of project
cd docker
docker-compose up

// for execute container with php
docker-compose exec app bash

// for execute container with mysql
docker-compose exec db bash

```

## Installation

```

composer install

php artisan key:generate

php artisan migtate --seed

```
